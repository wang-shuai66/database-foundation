# 数据类型

1. 布尔：bit
2. 整形：int,tinyint(0-255)
3. 浮点型：dedimal(5,2),numeric,最大精确38
4. 字符串：char,varchar,nvarchar 最大长度varchar(8000),nvarchar(4000)
5. 时间类型：date（2022-02-23），datetime(精确到毫秒2022-02-23-10:09:40)smalldatetime（秒级）
6. 文件类型：长度不够，nvarchar(max)

